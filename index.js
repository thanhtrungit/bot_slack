require('dotenv').config();
const { App } = require('@slack/bolt');
const { Configuration, OpenAIApi } = require("openai");

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
});
const openai = new OpenAIApi(configuration);


const app = new App({
  signingSecret: process.env.SLACK_SIGNING_SECRET,
  token: process.env.SLACK_BOT_TOKEN,
  appToken: process.env.SLACK_APP_TOKEN,
  socketMode: true,
});

/* Add functionality here */
// app.message(/^(hi|hello|hey).*/, async ({message, say}) => {
//     console.log(message)
//     await say(`Hello <@${message.user}>`)
// });

app.event('app_mention', async ({ event, context, client, say }) => {
  const regex = />(.*)/;
  const result = regex.exec(event.text);
  if (result) {
    const text_after_greater_than = result[1];
    const completion = await openai.createChatCompletion({
        model: "gpt-3.5-turbo",
        messages: [
          { role: "user", content: text_after_greater_than },
        ],
        max_tokens: 4000
      });
    await say(`<@${event.user}> ${completion.data.choices[0].message.content}`)
  } else {
    console.log("No match");
  }
});

(async () => {
  // Start the app
  await app.start(process.env.PORT || 3000);
  console.log('⚡️ Bolt app is running!');
})();