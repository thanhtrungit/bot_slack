require('dotenv').config();
const { App } = require('@slack/bolt');
const { Configuration, OpenAIApi } = require("openai");
const schedule = require('node-schedule');

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
});
const openai = new OpenAIApi(configuration);


const app = new App({
  signingSecret: process.env.SLACK_SIGNING_SECRET,
  token: process.env.SLACK_BOT_TOKEN,
  appToken: process.env.SLACK_APP_TOKEN,
  socketMode: true,
});

const fs = require('fs');

const filePaths = [
  'api.md',
  'artisan.md',
  'auth.md',
  'collections.md',
  'db-models-and-eloquent.md',
  'factories.md',
  'log-and-debug.md',
  'mail.md',
  'migrations.md',
  'models-relations.md',
  'other.md',
  'routing.md',
  'validation.md',
  'views.md',
];

// Hàm đọc nội dung tệp
function readFileContent(filePath) {
  return new Promise((resolve, reject) => {
    fs.readFile(`laravel-tips/${filePath}`, 'utf8', (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

// Hàm lấy nội dung ngẫu nhiên từ tệp dựa trên tiêu đề ###
async function getRandomContent() {
  const randomFilePath = filePaths[Math.floor(Math.random() * filePaths.length)];

  try {
    const fileContent = await readFileContent(randomFilePath);

    const regex = /^###\s+([\s\S]+?(?=\n\n|\n$))/gm;
    const titles = [];
    let match;
    
    while ((match = regex.exec(fileContent)) !== null) {
      titles.push(`### ${match[1]}`);
    }

    if (!titles) {
      throw new Error('Không tìm thấy tiêu đề ### trong tệp.');
    }

    const randomTitle = titles[Math.floor(Math.random() * titles.length)];
    const contentStartIndex = fileContent.indexOf(randomTitle);
    const contentEndIndex = titles.indexOf(randomTitle) !== titles.length - 1
      ? fileContent.indexOf(titles[titles.indexOf(randomTitle) + 1])
      : fileContent.length;

    const randomContent = fileContent.substring(contentStartIndex, contentEndIndex).trim();
    return randomContent;
  } catch (err) {
    console.error('Đã xảy ra lỗi:', err);
  }
}

async function sendMessageToSlack() {
  try {
    const channel = 'test_app'; // Replace with your Slack channel name
    let message = await getRandomContent()
    message = message.replace(/### (.*?)\n/g, (_, content) => `*${content}*\n`);
    message = message.replace(/```(.*?)\n/g, (_, content) => '```\n');
    message = '*#LaravelTipsDaily:* ' + message
    await app.client.chat.postMessage({
      channel: channel,
      text: message,
      mrkdwn: true
    });

    console.log('Message sent successfully to Slack!');
  } catch (error) {
    console.error('Error sending message to Slack:', error);
  }
}

const job = schedule.scheduleJob('* * * * *', function() {
  // Thực hiện công việc của bạn ở đây
  sendMessageToSlack();
});

// Call the function to send the message

(async () => {
  // Start the app
  await app.start(process.env.PORT || 3000);
  console.log('⚡️ Bolt app is running!');
})();